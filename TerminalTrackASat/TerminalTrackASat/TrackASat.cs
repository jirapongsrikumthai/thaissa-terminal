﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace TerminalTrackASat
{
    public partial class TrackASat : Form
    {
        //ScopeDome
        static ASCOM.DriverAccess.Dome mDome;
        private string mSelectedDome = "";
        bool scopeDomeConnect = false;

        //Telescope
        private ASCOM.DriverAccess.Telescope mTelescope;
        private string mSelectedTelescope = "";
        bool telescopeConnect = false;

        


        ////MQTT
        MqttClient client;
        delegate void SetTextCallback(string text);
        public TrackASat()
        {
            InitializeComponent();
        }

        private void TrackASat_Load(object sender, EventArgs e)
        {
            try
            {
                txtTerminal.Clear();
                OutputMsgTerminal("Started ! ! !");
                ConnectMqtt();
                timer.Start();
            }
            catch (Exception ex)
            {
                OutputMsgTerminal("Error:" + ex.Message.ToString());
            }
        }

        public void UpdateTextOfTerminal(string text)
        {
            if (this.txtTerminal.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(UpdateTextOfTerminal);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.txtTerminal.AppendText("[ Time " + String.Format(DateTime.Now.ToString("HH:mm:ss") + " Output ] >> {0}", text.ToString() + Environment.NewLine));
            }
        }

        #region ScopeDome

        private void SelectReplacementScopeDome()
        {
            try
            {
                mSelectedDome = ASCOM.DriverAccess.Dome.Choose("ASCOM.ScopeDomeUSBDome.Dome");
                UpdateTextOfTerminal("ScopeDome selected ASCOM.ScopeDomeUSBDome.Dome");
            }
            catch (Exception ex)
            {
                UpdateTextOfTerminal("Error:" + ex.Message.ToString());
            }
        }

        private void ConnectScopeDome()
        {
            mDome = new ASCOM.DriverAccess.Dome("ASCOM.ScopeDomeUSBDome.Dome");
            mDome.Connected = true;
        }

        #endregion

        #region Telescope

        private void SelectReplacementTelescope()
        {
            try
            {
                mSelectedTelescope = ASCOM.DriverAccess.Telescope.Choose("ASCOM.PWI4.Telescope");
                UpdateTextOfTerminal("Telescope selected ASCOM.PWI4.Telescope");
            }
            catch (Exception ex)
            {
                UpdateTextOfTerminal("Error:" + ex.Message.ToString());
            }
        }

        private void ConnectTelescope()
        {
            mTelescope = new ASCOM.DriverAccess.Telescope("ASCOM.PWI4.Telescope");
            mTelescope.Connected = true;
        }
        #endregion

        #region MQTTT

        private void ConnectMqtt()
        {
            string host = "164.115.43.87";
            client = new MqttClient(IPAddress.Parse(host));
            client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            client.Connect(Guid.NewGuid().ToString());
            OutputMsgTerminal("MQTT connected !");
            SubscribeMsgMQTT();
        }
        
        private void SubscribeMsgMQTT()
        {
            string location = "00152/01";
            if (client.IsConnected)
            {
                client.Subscribe(new String[] {
                    location + "/hardware/choose", location + "/hardware/connect" },
                    new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }
                );
                OutputMsgTerminal("MQTT subscribe success!");
            }
        }

        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string[] topic = e.Topic.Trim().Split('/');
            string message = Encoding.Default.GetString(e.Message).Trim().ToString();
            if (topic[2].ToLower().Trim() == "hardware")
            {
                if (topic[3].ToLower().Trim() == "choose")
                {
                    if (message.ToString() == "0")
                    {
                        SelectReplacementScopeDome();
                        client.Publish(e.Topic.Trim().ToString(), Encoding.ASCII.GetBytes("ScopeDome selected ASCOM.ScopeDomeUSBDome.Dome"));
                    }
                    else if (message.ToString() == "1")
                    {
                        SelectReplacementTelescope();
                        client.Publish(e.Topic.Trim().ToString(), Encoding.ASCII.GetBytes("Telescope selected ASCOM.PWI4.Telescope"));
                    }
                }
                else if (topic[3].ToLower().Trim() == "connect")
                {
                    if (message.ToString() == "0")
                    {
                        scopeDomeConnect = true;
                        client.Publish(e.Topic.Trim().ToString(), Encoding.ASCII.GetBytes("ScopeDome connected."));
                        
                    }
                    else if (message.ToString() == "1")
                    {
                        telescopeConnect = true;
                        client.Publish(e.Topic.Trim().ToString(), Encoding.ASCII.GetBytes("Telescope connected."));
                    }
                }
            }
        }
        #endregion

        #region Tool
        private void OutputMsgTerminal(string msg)
        {
            txtTerminal.AppendText(String.Format("[ Time " + DateTime.Now.ToString("HH:mm:ss") + " , Output ] >> {0}", msg + Environment.NewLine));
        }


        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectScopeDome();
        }

        private void timerTick(object sender, EventArgs e)
        {
            if (scopeDomeConnect)
            {
                scopeDomeConnect = false;
                ConnectScopeDome();
                UpdateTextOfTerminal("ScopeDome connected!");
            }

            if(telescopeConnect)
            {
                telescopeConnect = false;
                ConnectTelescope();
                UpdateTextOfTerminal("Telescope connected!");
            }
        }
    }
}